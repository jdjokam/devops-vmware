---
- name: VM configurations
  hosts: "{{ server_ip }}"
  become: yes
  collections:
    - ansible.posix
    - ansible.builtin
  vars_files:
    host_vars/parameters.yml
  vars:
    ansible_python_interpreter: /usr/bin/python3.6

  tasks:
  - name: Set a hostname
    hostname:
      name: "{{ vmhostname }}"

  - name: Add domain to resolv.conf
    blockinfile:
      path: /etc/resolv.conf
      block: |
        {{ net_domains }}
        nameserver {{ dns1 }}
        nameserver {{ dns2 }}

  - name: Persistent nics udev config
    blockinfile:
      path: "{{ udev_nic }} "
      block: |
        SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="{{ ansible_ens224.macaddress }}",     ATTR{type}=="1", NAME="eth0"
        SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="{{ ansible_ens256.macaddress }}",     ATTR{type}=="1", NAME="eth1"
      owner: root
      group: root
      mode: 0644
      create: yes
    ignore_errors: yes
    when: ansible_distribution_major_version|int == 7

  - name: Persistent nic1 udev config
    blockinfile:
      path: "{{ nic1 }}"
      block: |
        DEVICE=eth0
        NAME=eth0
        ONBOOT=yes
        MTU=1500
        HWADDR={{ ansible_ens224.macaddress }}
        TYPE=Ethernet
        BOOTPROTO=none
        IPADDR={{ server_ip }}
        NETMASK={{ netmask }}
        GATEWAY={{ gateway }}
        USERCTL=no
        USERS=root
        NM_CONTROLLED="no"
        IPV4_FAILURE_FATAL=yes
        IPV6INIT=no
        IPV6_AUTOCONF=yes
        IPV6_DEFROUTE=yes
        IPV6_PEERDNS=yes
        IPV6_PEERROUTES=yes
        IPV6_FAILURE_FATAL=no
        IPV6_ADDR_GEN_MODE=stable-privacy
      owner: root
      group: root
      mode: 0644
      create: yes
    ignore_errors: yes
    when: ansible_distribution_major_version|int == 7

  - name: Persistent nic2 udev config
    blockinfile:
      path: "{{ nic2 }}"
      block: |
        DEVICE=eth1
        NAME=eth1
        ONBOOT=yes
        MTU=9000
        HWADDR={{ ansible_ens256.macaddress }}
        TYPE=Ethernet
        BOOTPROTO=none
        IPADDR={{ nas_ip }}
        NETMASK={{ nas_netmask }}
        USERCTL=no
        USERS=root
        NM_CONTROLLED="no"
        IPV4_FAILURE_FATAL=yes
        IPV6INIT=no
        IPV6_AUTOCONF=yes
        IPV6_DEFROUTE=yes
        IPV6_PEERDNS=yes
        IPV6_PEERROUTES=yes
        IPV6_FAILURE_FATAL=no
        IPV6_ADDR_GEN_MODE=stable-privacy
      owner: root
      group: root
      mode: 0644
      create: yes
    ignore_errors: yes
    when: ansible_distribution_major_version|int == 7

  - name: remove old nics
    command: "rm -f /etc/sysconfig/network-scripts/{{ item }}"
    loop:
      - "ifcfg-ens224"
      - "ifcfg-ens256"
    ignore_errors: yes
    when: ansible_distribution_major_version|int == 7

- name: system reboot
  hosts: "{{ server_ip }}"
  become: yes
  collections:
    - ansible.builtin

  tasks:
  - name: reboot server 1 
    block:
      - name: 
        shell: sleep 2 && shutdown -r now "Ansible triggered reboot"
        async: 1
        poll: 0
        ignore_errors: true

      - name: Wait for system to boot up
        local_action:
          module: wait_for
          host: "{{ hostvars[inventory_hostname]['ansible_host'] | default(hostvars[inventory_hostname]['ansible_ssh_host'], true) | default(inventory_hostname, true) }}"
          port: "{{ hostvars[inventory_hostname]['ansible_port'] | default(hostvars[inventory_hostname]['ansible_ssh_port'], true) | default('22', true) }}"
          delay: 15
          timeout: 240
        become: false

- name: update net conf
  hosts: "{{ server_ip }}"
  become: yes
  collections:
    - ansible.builtin
  vars_files:
    host_vars/parameters.yml
  vars:
    ansible_python_interpreter: /usr/bin/python3.6

  tasks:
  - name: delete incorrect network conf file
    file:
      path: "{{ nw_conf }}"
      state: absent

  - name: update network conf files
    blockinfile:
      path: "{{ nw_conf }}"
      block: |
        GATEWAY= {{gateway }}
        IPV6INIT=no
        NETWORKING=yes
        NOZEROCONF=yes
      owner: root
      group: root
      mode: 0644
      create: yes
    ignore_errors: yes

  - name: Reboot system 2
    shell: sleep 2 && shutdown -r now "Ansible triggered reboot"
    async: 1
    poll: 0
    ignore_errors: true
